Source: fasttree
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Thorsten Alteholz <debian@alteholz.de>,
           Andreas Tille <tille@debian.org>,
           Roland Fehrenbacher <rf@q-leap.de>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/fasttree
Vcs-Git: https://salsa.debian.org/med-team/fasttree.git
Homepage: http://www.microbesonline.org/fasttree/
Rules-Requires-Root: no

Package: fasttree
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: phylogenetic trees from alignments of nucleotide or protein sequences
 FastTree infers approximately-maximum-likelihood phylogenetic trees from
 alignments of nucleotide or protein sequences. It handles alignments
 with up to a million of sequences in a reasonable amount of time and
 memory. For large alignments, FastTree is 100-1,000 times faster than
 PhyML 3.0 or RAxML 7.
 .
 FastTree is more accurate than PhyML 3 with default settings, and much
 more accurate than the distance-matrix methods that are traditionally
 used for large alignments. FastTree uses the Jukes-Cantor or generalized
 time-reversible (GTR) models of nucleotide evolution and the JTT
 (Jones-Taylor-Thornton 1992) model of amino acid evolution. To account
 for the varying rates of evolution across sites, FastTree uses a single
 rate for each site (the "CAT" approximation). To quickly estimate the
 reliability of each split in the tree, FastTree computes local support
 values with the Shimodaira-Hasegawa test (these are the same as PhyML 3's
 "SH-like local supports").
 .
 This package contains a single threaded version (fasttree) and a
 parallel version which uses OpenMP (fasttreMP).
